﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
    private const float DEFAULT_CAMERA_SENSITIVITY = 60f;

    public Transform Target;
    public float CameraSensitivity = DEFAULT_CAMERA_SENSITIVITY;
    public bool InvertVerticalAxis = false;
    
    private int verticalInvert;
    private const float verticalLimit = 85;
    private const float angularPeriod = 360;
    
    private Vector3 trackingVector;
    private Vector3 right;
    
    private float vRot = 0;
    private float hRot = 0;
    private float vRotDelta = 0;
    private float hRotDelta = 0;
    
    // Use this for initialization
    void Start () 
    {
        verticalInvert = (InvertVerticalAxis) ? -1 : 1;
        
        if (CameraSensitivity==0) CameraSensitivity=DEFAULT_CAMERA_SENSITIVITY;
        
        trackingVector = Target.position - this.transform.position;		
    }
    
    // Update is called once per frame
    void Update () 
    {			
        
    }	
    
    private void LateUpdate()
    {	
        updateAnglesFromInput();				
            
        setTrackingVectorLength();

        //update camera tracking vector as per angle changes
        rotateTrackingVector();
        
        //move camera relative to target's position using tracking vector
        this.transform.position = Target.position-trackingVector;
        
        //rotate camera to look at target using tracking vector
        this.transform.rotation = Quaternion.LookRotation(trackingVector);
        
        //debug
        //Debug.Log(vRot.ToString()+","+hRot.ToString()+","+trackingVector.ToString());
    }
    
    #region PrivateMethods
    
    private float getHorizontalAngleChange()
    {		
        return Input.GetAxis("Mouse X")*CameraSensitivity*Time.deltaTime; 
    }

    private float getVerticalAngleChange()
    {
        return Input.GetAxis("Mouse Y")*CameraSensitivity*verticalInvert*Time.deltaTime;
    }	
    
    private float clampAngle(float angle)
    {
        if (angle < -angularPeriod) angle += angularPeriod;
        if (angle > angularPeriod) angle -= angularPeriod;		
        return angle;
    }
    
    private void updateAnglesFromInput()
    {
        //vertical angle
        vRotDelta = getVerticalAngleChange();
        if (Mathf.Abs(vRotDelta+vRot) > verticalLimit) vRotDelta=0;
        vRot = clampAngle(vRot + vRotDelta);
        
        //horizontal angle
        hRotDelta = getHorizontalAngleChange();
        hRot = clampAngle(hRot + hRotDelta);
    }
    
    private void rotateTrackingVector()
    {
        //horizontal is relative to tracked object
        trackingVector = Quaternion.Euler(0,hRotDelta,0) * trackingVector;		
        
        //vertical needs to happen in axis of current horizontal rotation
        right = Vector3.Cross(trackingVector,Vector3.up);
        trackingVector = Quaternion.AngleAxis(vRotDelta,right) * trackingVector;				
    }

    private void setTrackingVectorLength()
    {
        trackingVector *= 1 - Input.GetAxis("Mouse ScrollWheel");
    }
    
    #endregion
    
}
